import { defineStore } from 'pinia';
import axios from 'axios';
const baseUrl = `${import.meta.env.VITE_API_URL}/task`;

export const useTaskStore = defineStore("task", {
    id: 'task',
    state: () => ({
        // initialize state from local storage to enable user to stay logged in
        user: JSON.parse(localStorage.getItem('user')),
        srftoken: '',
        tasks: [],
        ingresos: 0,
        egresos: 0,
        saldo: 0,
        ahorro: 0,
        descuentoAhorro: 0,
        submitting: false,

        errors: ''
    }),
    actions: {

        getAll() {
            this.srftoken = JSON.parse(localStorage.getItem('srftoken'));
            axios.get(`${baseUrl}/`, { headers: { Authorization: `Bearer ${this.srftoken}` } })
                .then(RESPONSE => {
                    this.tasks = RESPONSE.data.tasks.data;
                    this.ingresos = this.tasks.reduce(function (sum, tax) {
                        return sum + tax.ingresos;
                    }, 0);
                    this.egresos = this.tasks.reduce(function (sum, tax) {
                        return sum + tax.egresos;
                    }, 0);
                    this.ahorro = this.tasks.reduce(function (sum, tax) {
                        return sum + tax.ahorro;
                    }, 0);
                    this.descuentoAhorro = this.tasks.reduce(function (sum, tax) {
                        return sum + tax.descuentoAhorro;
                    }, 0);

                    this.saldo = this.ingresos - this.egresos;
                    this.ahorro = this.ahorro - this.descuentoAhorro;

                }).catch(ERROR => {

                    console.log(ERROR);

                }).then(() => {

                });
        },

        async store(datos) {
            this.srftoken = JSON.parse(localStorage.getItem('srftoken'));
            return await axios.post(`${baseUrl}`, {
                "title": datos.title,
                "start": datos.start,
                "end": datos.start,
                "color": datos.color,
                "allDay": datos.allDay,
                "className": datos.className,
                "extendedProps": {
                    "tipo": datos.extendedProps.tipo,
                    "monto": datos.extendedProps.monto,
                    "nota": datos.extendedProps.nota,
                    "color": datos.color,
                    "className": datos.className,
                }
            }, { headers: { Authorization: `Bearer ${this.srftoken}` } })
                .then(RESPONSE => {

                }).catch(ERROR => {

                    console.log(ERROR.response.data.errors);
                    throw ERROR;

                });
        },

        async update(datos, id) {
            this.srftoken = JSON.parse(localStorage.getItem('srftoken'));
            return await axios.post(`${baseUrl}/update/${id}`, {
                "title": datos.title,
                "start": datos.start,
                "end": datos.start,
                "color": datos.color,
                "allDay": datos.allDay,
                "className": datos.className,
                "extendedProps": {
                    "tipo": datos.extendedProps.tipo,
                    "monto": datos.extendedProps.monto,
                    "nota": datos.extendedProps.nota,
                    "color": datos.color,
                    "className": datos.className,
                }
            }, { headers: { Authorization: `Bearer ${this.srftoken}` } })
                .then(RESPONSE => {

                }).catch(ERROR => {

                    console.log(ERROR);
                    throw ERROR

                });
        },
        //elimiar
        async destroy(id) {
             this.srftoken = JSON.parse(localStorage.getItem('srftoken'));
         return await   axios.delete(`${baseUrl}/${id}`, { headers: { Authorization: `Bearer ${this.srftoken}` } })
                .then(RESPONSE => {

                }).catch(ERROR => {

                    console.log(ERROR);
                    throw ERROR;
                });
        },
        resetTasks() {
            this.tasks = [];
            this.saldo = 0;
        }

    },

    getters: {
        getTasks(state) {
            return state.tasks;
        },
        getSubmit(state) {
            return state.submitting;
        },



    },


})



